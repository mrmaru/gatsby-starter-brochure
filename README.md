# gatsby-starter-default
The Gatsby Brochure starter

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/)

Install this starter (assuming Gatsby is installed) by running from your CLI:
```
gatsby new https://bitbucket.com/mrmaru/gatsby-starter-brochure.git
```

## Deploy

create docker deploy strategy
