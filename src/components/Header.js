import React from 'react'
import Link from 'gatsby-link'
import Menu from './Menu/'
import SelectLanguage from './SelectLanguage'

const Header = (props) => (
  <div
    style={{
      background: '#333',
      marginBottom: '1.45rem',
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '1.45rem 1.0875rem',
      }}
    >
      <Menu langs={props.langs} />
      <SelectLanguage langs={props.langs} />
    </div>
  </div>
)

export default Header;
