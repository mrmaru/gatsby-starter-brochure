import React from "react"
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import { FormattedMessage } from 'react-intl';

import styles from './index.module.css'

const NavItem = ({ linkTo, children }) => (
  <li className={styles.links}>
    <button onClick={() => {
      const el = document.getElementById(linkTo)
      if(el !== null) {
        el.scrollIntoView({ behavior: 'smooth' })
      }
    }} >
      <FormattedMessage id={children}/>
    </button>
  </li>
)

export default class Index extends React.Component {

  constructor() {
    super()
    let _window
    if(typeof window !== 'undefined') {
      _window = window.innerWidth 
    } else {
      _window = { innerWidth : 0 }
    }
    const isMobile = _window.innerWidth > 767 ? false : true
    this.state = { showMobile: isMobile, showMenu: isMobile ? false : true, width: _window.innerWidth }
  }

  redraw = () => { 
    const isMobile = window.innerWidth > 767 ? false : true
    this.setState({showMobile: isMobile })
    this.setState({showMenu: isMobile ? false : true })
  }

  _throttle = (callback, limit) => {
    var wait = false;                  
    return function () {              
      if (!wait) {                   
        callback.call();         
        wait = true;               
        setTimeout(function () {  
          wait = false;         
        }, limit);
      } 
    }
  }

  // Should the resize function be throttled ?
  // https://stackoverflow.com/questions/27078285/simple-throttle-in-js
  componentDidMount() {
    this.redraw()
    window.addEventListener('resize', this.redraw)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.redraw)
  }

  render() {
    const display = this.state.showMenu ? 'inline' : 'none'
    return (
      <div role="navigation" className={styles.navbar} >
        <Link to="/">
          <h1> Site </h1>
        </Link>
        <button style={{display: this.state.showMobile ? 'inline' : 'none'}} 
          onClick={() => this.setState({ showMenu: this.state.showMenu ? false : true })}>
          <i className="fa fa-bars"></i>
        </button>
        <ul className={styles.menu} style={{display: display}}>
          <button style={{display: this.state.showMobile ? 'inline' : 'none'}} 
            onClick={() => this.setState({ showMenu: this.state.showMenu ? false : true })}>
            <i className="fa fa-close"></i>
          </button>

          <NavItem linkTo="/menu-item/">Menu Item</NavItem>
          <a href="#test">test Item</a>
        </ul>
      </div>
    )
  }
}
