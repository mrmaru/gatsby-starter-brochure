import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Starter Page</h1>
    <p>Welcome to your new Gatsby brochure site.</p>
  </div>
)

export default IndexPage
